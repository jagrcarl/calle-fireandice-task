package se.experis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Program {
    public static JSONObject getJsonByURL(String urlInput) {   //method to connect   (when JSONObject)
        JSONObject json = new JSONObject();
        try {
            URL url = new URL(urlInput);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputline;
                StringBuffer content = new StringBuffer();
                while ((inputline = br.readLine()) != null) {
                    content.append(inputline);
                }
                br.close();
                json = new JSONObject(content.toString());
            }
        } catch (Exception ex) {
            System.out.println("Error" + ex.getMessage());
        }
        return json;
    }

    public static JSONArray getJsonArrayByURL(String urlInput) {  //method to connect (when JSONArray)
        JSONArray jsonArray = new JSONArray();
        try {
            URL url = new URL(urlInput);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputline;
                StringBuffer content = new StringBuffer();
                while ((inputline = br.readLine()) != null) {
                    content.append(inputline);
                }
                br.close();
                jsonArray = new JSONArray(content.toString());
            }
        } catch (Exception ex) {
            System.out.println("Error" + ex.getMessage());
        }
        return jsonArray;
    }

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(System.in);
            String characterNr;
            System.out.println("Type the character number you want to see: ");
            characterNr = scan.nextLine();

            System.out.println("Name: " + getJsonByURL("https://anapioficeandfire.com/api/characters/" + characterNr).get("name")); //prints all info about the chosen character
            System.out.println("Gender: " + getJsonByURL("https://anapioficeandfire.com/api/characters/" + characterNr).get("gender"));
            System.out.println("Born: " + getJsonByURL("https://anapioficeandfire.com/api/characters/" + characterNr).get("born"));

            System.out.println("Do you want to see all sworn members of this character? Y / N");
            String answerMembers;
            answerMembers = scan.nextLine();

            if (answerMembers.equalsIgnoreCase("Y")) {
                JSONArray allegiances = (JSONArray) getJsonByURL("https://anapioficeandfire.com/api/characters/" + characterNr).get("allegiances"); //reads and stores allegiances into an array
                for (Object allegiance : allegiances) {
                    getJsonByURL((String) allegiance); //for every array object in alliegance, a connect will be created

                    JSONArray swornMembers = (JSONArray) getJsonByURL((String) allegiance).get("swornMembers");    //stores sworn members

                    for (Object swornMember : swornMembers) {
                        getJsonByURL((String) swornMember);         //for every sworn memeber, a connect will be created

                        Object name = getJsonByURL((String) swornMember).get("name");   //stores all name for sworn members
                        System.out.println(name);    //and then prints it
                    }
                }
            } else if (answerMembers.equalsIgnoreCase("N")) {   //if you type N or anything else, it will not check for sworn members
                System.out.println("Ok.\n");
            } else {
                System.out.println("You didn't type Y or N.\n");
            }

            getJsonArrayByURL("https://anapioficeandfire.com/api/books");           //connection to books

            JSONArray json4 = new JSONArray(getJsonArrayByURL("https://anapioficeandfire.com/api/books").toString(9)); //stores all information from books
            ArrayList<JSONObject> booksByBantam = new ArrayList<>(); //creates a arraylist that will handle books published by bantam books
            for (Object o : json4) {
                JSONObject json5 = new JSONObject(o.toString());
                String publisher = json5.getString("publisher");          //stores all information about publisher in all books
                if (publisher.equals("Bantam Books")) {
                    booksByBantam.add(json5);       //if the publisher is batman books, add to array
                }
            }



            for (JSONObject jo : booksByBantam) {
                     System.out.println("--------------------------------------------------------------------------");
                     System.out.println("                          " + jo.getString("name"));
                    System.out.println("--------------------------------------------------------------------------");   //for each object that has bantan book , print their name
                JSONArray json5 = jo.getJSONArray("povCharacters");
                for(Object o : json5) {
                    getJsonByURL((String) o);      //for each object with pov character
                     Object name = getJsonByURL((String) o).get("name");               //store all name for pov char
                    System.out.println(name);                                                                       //and print
                }
                System.out.println();
                
            }

        }catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
